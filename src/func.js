const getSum = (str1, str2) => {
  if(typeof(str1) != "string" || typeof(str2) != "string" 
  || (str1.length != 1 && isNaN(str1)) || (str2.length != 1 && isNaN(str2))) return false;
  
  let num1;
  let num2;
  if(str1 == "") num1 = 0;
  else num1 = parseInt(str1);
  if(str2 == "") num2 = 0;
  else num2 = parseInt(str2);
  
  return (num1 + num2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let cntPosts = 0;
  let cntComments = 0;
  listOfPosts.forEach((obj) => {
	  if(obj.author == authorName) cntPosts++;
	  if(obj.comments != null){
		obj.comments.forEach((cmt) => {
			if(cmt.author == authorName) cntComments++;
		});
	  }
  });
  return "Post:" + cntPosts + ",comments:" + cntComments;
};

const tickets=(people)=> {
  let bills25 = 0;
  let bills50 = 0;
  let result = "YES";
  people.forEach((person) => {
	if(person == 25) bills25++;
	else if(person == 50) {
		bills50++;
		if(bills25 == 0) result = "NO";
		bills25--;
	}
	// if person == 100
	else {
		if(bills50 > 0 && bills25 > 0) {
			bills50--;
			bills25--;
		}
		else if(bills25 > 2) {
			bills25 -= 3;
		}
		else result = "NO";
	}
  });
  return result;
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
